" LOAD PATHOGEN FIRST
execute pathogen#infect()

" FILE TYPES
filetype on

"THEME & LAYOUT
set background=dark
colorscheme gruvbox 
let g:solarized_termtrans=1
set lines=35 columns=100
set colorcolumn=80
set number
syntax on

if has('win32')
    set guifont=Consolas:h10:cANSI:qDRAFT
endif

"Sane tab handling
:set tabstop=4
:set shiftwidth=4
:set expandtab

" RELOAD VIMRC
map <leader>s :source ~/.vimrc<CR>

" Acivate code formatting for rust from rust.vim
let g:rustfmt_autosave = 1

" Powerline
set rtp+=~/.vim/bundle/powerline/powerline/bindings/vim

" Syntastic
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

let g:syntastic_python_checkers = ['flake8']

" Racer
set hidden
let g:racer_cmd = "/home/peter/.cargo/bin/racer"
let $RUST_SRC_PATH="/home/peter/.multirust/toolchains/stable-x86_64-unknown-linux-gnu/lib/rustlib/src/rust/src"
